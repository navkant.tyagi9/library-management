from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Member(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='member')
    mobile = models.CharField(blank=True, null=True, max_length=10, help_text='Maximum 10 digits.')
    aadhar = models.CharField(blank=True, null=True, max_length=12, help_text='Maximum 12 digits.')
    amount_due = models.IntegerField(default=0)


# @receiver(post_save, sender=User)
# def create_user_profile(sender, instance, created, **kwargs):
#     if created:
#         print('user is created')
#         Member.objects.create(user=instance)
#         # print('member is created')
#         # print(f'mobile is {instance.member.mobile} and aadhaar is {instance.member.aadhar}')
#
#
# @receiver(post_save, sender=User)
# def save_user_profile(sender, instance, **kwargs):
#     instance.member.save()
