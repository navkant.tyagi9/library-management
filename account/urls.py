from django.conf.urls import url, include
from account import views

urlpatterns = [

    url(r'register', views.register, name='signup'),

    url(r'', include('django.contrib.auth.urls')),

]