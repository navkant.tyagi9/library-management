from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from account.models import Member


class UserForm(UserCreationForm):

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')


class MemberForm(forms.ModelForm):

    class Meta:
        model = Member
        fields = ('mobile', 'aadhar')
