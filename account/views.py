from account.forms import UserForm, MemberForm
from account.models import Member
from django.contrib import messages
from django.contrib.auth import login, authenticate
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.utils.text import _


def register(request):
    if request.method == 'POST':
        user_form = UserForm(request.POST)
        member_form = MemberForm(request.POST)
        if user_form.is_valid() and member_form.is_valid():
            user = user_form.save()
            mobile = member_form.cleaned_data['mobile']
            aadhar = member_form.cleaned_data['aadhar']
            member = Member(user=user, mobile=mobile, aadhar=aadhar)
            member.save()

            return redirect('index')
        else:
            pass
            messages.error(request, _('Please correct the error below.'))
    else:
        user_form = UserForm()
        member_form = MemberForm()
    return render(request, 'account/signup.html', {
        'user_form': user_form,
        'member_form': member_form
    })
