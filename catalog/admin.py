from catalog.models import Author, Genre, Book, BookInstance, Language
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from django.contrib import admin


admin.site.register(Genre)
admin.site.register(Language)


class AuthorResource(resources.ModelResource):
    class Meta:
        model = Author


class BooksInline(admin.TabularInline):
    model = Book


class AuthorResourceAdmin(ImportExportModelAdmin):
    list_display = ('last_name', 'first_name', 'date_of_birth', 'date_of_death')
    fields = ['first_name', 'last_name', ('date_of_birth', 'date_of_death')]
    inlines = [BooksInline]

    resource_class = AuthorResource


# Register the admin class with the associated model
admin.site.register(Author, AuthorResourceAdmin)


class BooksInstanceInline(admin.TabularInline):
    model = BookInstance

# Register the Admin classes for Book using the decorator
@admin.register(Book)  # this does exactly the same thing as the admin.site.register() syntax
class BookAdmin(admin.ModelAdmin):
    list_display = ('title', 'author','display_genre')
    inlines = [BooksInstanceInline]


# Register the Admin classes for BookInstance using the decorator
@admin.register(BookInstance)
class BookInstanceAdmin(admin.ModelAdmin):
    list_filter = ('status', 'due_back')
    list_display = ('name', 'status', 'due_back', 'borrower')
    fieldsets = (
        ('Book Details', {
            'fields': ('book', 'imprint', 'id')
        }),
        ('Availability', {
            'fields': ('status', 'due_back', 'borrower')
        }),
    )







