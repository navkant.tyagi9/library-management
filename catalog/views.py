import datetime

from catalog import forms
from catalog.models import Author, Book, BookInstance, Genre, Language
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.views.generic import CreateView, UpdateView, DeleteView
from django.shortcuts import render, get_object_or_404, HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, DetailView


# --------------------------  TITLES SECTION  --------------------------


def index(request):
    """
    View function for home page of site.
    """
    # Generate counts of some of the main objects
    num_books = Book.objects.all().count()
    num_instances = BookInstance.objects.all().count()

    #  Number of visits to this view
    num_visits = request.session.get('num_visits', 0)
    request.session['num_visits'] = num_visits + 1

    # Available books (status = 'a')
    num_instances_available = BookInstance.objects.filter(status__exact='a').count()
    num_authors = Author.objects.count()  # The 'all()' is implied by default.

    # Genre Count
    genre_num = Genre.objects.count()

    # Books with "The" in title
    the_in_title = Book.objects.filter(title__contains='The ')

    # Render the HTML template index.html with the data in the context variable
    return render(
        request,
        'catalog/index.html',
        context={'num_books': num_books, 'num_instances': num_instances,
                 'num_instances_available': num_instances_available, 'num_authors': num_authors,
                 'genre_num': genre_num, 'the_in_title': the_in_title, 'the_in_title': the_in_title,
                 'num_visits': num_visits},
    )


@login_required
def renew_book_librarian(request, pk):
    print('in renew view: ', pk)
    book_inst = get_object_or_404(BookInstance, pk=pk)
    permission_required = 'catalog.can_see_all_borrowed_books'

    # If the request is POST process the form data
    if request.method == 'POST':

        # Create a form instance and populate it with data from request (binding)
        form = forms.RenewBookModelForm(request.POST)

        # Check if form is valid
        if form.is_valid():
            book_inst.due_back = form.cleaned_renewal_date()
            book_inst.save()
        else:
            proposed_renewal_date = datetime.date.today() + datetime.timedelta(weeks=3)
            form = forms.RenewBookModelForm(initial={'renewal_date': proposed_renewal_date})
            return render(request, 'catalog/books/book_renew_librarian.html', {'form': form, 'bookinst': book_inst})

        # Redirect to new url
        return HttpResponseRedirect(reverse_lazy('all_borrowed'))

    else:
        proposed_renewal_date = datetime.date.today() + datetime.timedelta(weeks=3)
        form = forms.RenewBookModelForm(initial={'renewal_date': proposed_renewal_date})

    return render(request, 'catalog/books/book_renew_librarian.html', {'form': form, 'bookinst': book_inst})


class BookListView(ListView):
    model = Book
    paginate_by = 3
    template_name = 'catalog/books/book_list.html'

    # context_object_name = 'my_book_list'   # your own name for the list as a template variable
    # queryset = Book.objects.filter(title__icontains='war')[:5] # Get 5 books containing the title war
    # template_name = 'books/my_arbitrary_template_name_list.html'  # Specify your own template name/location

    def get_context_data(self):
        # Call the base implementation first to get the context
        context = super().get_context_data()
        # Create any data and add it to the context
        context['extra_data'] = 'This is just some extra data'
        return context


class BookDetailView(DetailView):
    model = Book
    template_name = 'catalog/books/book_detail.html'


class LoanedBooksByUserListView(LoginRequiredMixin, ListView):
    """
    Generic class-based view listing books on loan to current user.
    """
    model = BookInstance
    template_name = 'catalog/books/bookinstance_list_borrowed_user.html'
    paginate_by = 3

    def get_queryset(self):
        try:
            return BookInstance.objects.filter(borrower=self.request.user).filter(status__exact='o').order_by('due_back')
        except:
            print('Login to see your book')


class AllBooksLoanedListView(PermissionRequiredMixin, ListView):
    permission_required = 'catalog.can_see_all_borrowed_books'
    model = BookInstance
    template_name = 'catalog/books/all_books_borrowed.html'
    # paginate_by = 3

    def get_queryset(self):
        return BookInstance.objects.filter(status__exact='o')


class BookCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'catalog.can_see_all_borrowed_books'
    model = Book
    fields = '__all__'
    template_name = 'catalog/books/book_form.html'


class BookUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'catalog.can_see_all_borrowed_books'
    model = Book
    fields = '__all__'
    template_name = 'catalog/books/book_form.html'


class BookDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'catalog.can_see_all_borrowed_books'
    model = Book
    success_url = reverse_lazy('books')


# --------------------------  AUTHOR SECTION  --------------------------


class AuthorCreateView(PermissionRequiredMixin, CreateView):
    form_class = forms.AuthorDOBForm
    permission_required = 'catalog.can_see_all_borrowed_books'
    model = Author
    initial = {'date_of_death': '05/05/2018'}
    template_name = 'catalog/authors/author_form.html'

    def form_valid(self, form):
        print('form validation!!')
        self.date_of_birth = form.cleaned_dob()
        return super().form_valid(form)


class AuthorUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'catalog.can_see_all_borrowed_books'
    model = Author
    fields = ['first_name', 'last_name', 'date_of_birth', 'date_of_death']
    template_name = 'catalog/authors/author_form.html'


class AuthorDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'catalog.can_see_all_borrowed_books'
    model = Author
    template_name = 'catalog/authors/author_confirm_delete.html'
    success_url = reverse_lazy('authors')


class AuthorListView(ListView):
    model = Author
    template_name = 'catalog/authors/author_list.html'


class AuthorDetailView(DetailView):
    model = Author
    template_name = 'catalog/authors/author_detail.html'


# --------------------------  GENRE SECTION  --------------------------


class GenreListView(ListView):
    model = Genre
    template_name = 'catalog/genres/genre_list.html'

# def genrelistview(request):
#     genres = Genre.objects.all()


# class GenreDetailView(DetailView):
#     model = Genre
#     template_name = 'catalog/genres/genre_detail.html'


def genredetailview(request, pk):
    genre = Genre.objects.get(pk=pk)
    books = Book.objects.filter(genre=genre)

    context = {'genre': genre, 'books': books}

    return render(request, 'catalog/genres/genre_detail.html', context)


class GenreCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'catalog.can_see_all_borrowed_books'
    model = Genre
    fields = '__all__'
    template_name = 'catalog/genres/genre_form.html'


class GenreUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'catalog.can_see_all_borrowed_books'
    model = Genre
    fields = '__all__'
    template_name = 'catalog/genres/genre_form.html'


class GenreDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'catalog.can_see_all_borrowed_books'
    model = Genre
    template_name = 'catalog/genres/genre_confirm_delete.html'
    success_url = reverse_lazy('genres')


# --------------------------  LANGUAGE SECTION  --------------------------


class LanguageCreateView(LoginRequiredMixin, CreateView):
    model = Language
    fields = '__all__'
    template_name = 'catalog/languages/language_form.html'


# --------------------------  BOOK INSTANCE SECTION  --------------------------


class BookInstanceCreateView(CreateView):
    model = BookInstance
    fields = '__all__'
    template_name = 'catalog/bookinstances/bookinstance_form.html'







