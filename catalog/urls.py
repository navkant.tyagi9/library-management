from django.conf.urls import url
from catalog import views

urlpatterns = [

    # Titles
    url(r'books$', views.BookListView.as_view(), name='books'),
    url(r'book/(?P<pk>\d+)$', views.BookDetailView.as_view(), name='book_detail'),
    url(r'books_borrowed$', views.LoanedBooksByUserListView.as_view(), name='my_borrowed'),
    url(r'all_borrowed$', views.AllBooksLoanedListView.as_view(), name='all_borrowed'),
    url(r'book/([0-9a-f-]+)/renew$', views.renew_book_librarian, name='renew-book-librarian'),
    url(r'book/create$', views.BookCreateView.as_view(), name='book_create'),
    url(r'book/(?P<pk>\d+)/update$', views.BookUpdateView.as_view(), name='book_update'),
    url(r'book/(?P<pk>\d+)/delete$', views.BookDeleteView.as_view(), name='book_delete'),

    # Authors
    url(r'authors$', views.AuthorListView.as_view(), name='authors'),
    url(r'author/(?P<pk>\d+)$', views.AuthorDetailView.as_view(), name='author_detail'),
    url(r'author/create$', views.AuthorCreateView.as_view(), name='author_create'),
    url(r'author/(?P<pk>\d+)/update$', views.AuthorUpdateView.as_view(), name='author_update'),
    url(r'author/(?P<pk>\d+)/delete$', views.AuthorDeleteView.as_view(), name='author_delete'),

    # Genres
    url(r'genres$', views.GenreListView.as_view(), name='genres'),
    url(r'genre/(?P<pk>\d+)$', views.genredetailview, name='genre_detail'),
    url(r'genre/create$', views.GenreCreateView.as_view(), name='genre_create'),
    url(r'genre/(?P<pk>\d+)/update$', views.GenreUpdateView.as_view(), name='genre_update'),
    url(r'genre/(?P<pk>\d+)/delete$', views.GenreDeleteView.as_view(), name='genre_delete'),

    # Language
    url(r'language/create$', views.LanguageCreateView.as_view(), name='language_create'),

    # BookInstances
    url(r'bookinstance/create$', views.BookInstanceCreateView.as_view(), name='bookinstance_create'),


    url(r'', views.index, name='index'),

]