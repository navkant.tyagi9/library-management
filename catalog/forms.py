import datetime

from catalog.models import BookInstance, Author
from django import forms
from django.core.exceptions import ValidationError
import pytz


# class RenewBookForm(forms.Form):
#
#     renewal_date = forms.DateTimeField(help_text='Enter a date between now and 3 weeks(default 4 weeks)')
#
#     def clean_renewal_data(self):
#         utc = pytz.UTC
#         data = self.cleaned_data['renewal_date']
#         print(data)
#         if data < utc.localize(datetime.datetime.today()):
#             raise ValidationError('Invalid date - renewal in past')
#
#         if data > utc.localize(datetime.datetime.today()) + datetime.timedelta(weeks=3):
#             raise ValidationError('Invalid date - renewal more than 4 weeks ahead')
#
#         return data


class RenewBookModelForm(forms.ModelForm):

    def cleaned_renewal_date(self):
        data = self.cleaned_data['due_back']

        # Check date is not in past.
        if data < datetime.date.today():
            raise ValidationError('Invalid date - renewal in past')

        # Check date is in range librarian allowed to change (+4 weeks)
        if data > datetime.date.today() + datetime.timedelta(weeks=4):
            raise ValidationError('Invalid date - renewal more than 4 weeks ahead')

        # Remember to always return the cleaned data.
        return data

    class Meta:
        model = BookInstance
        fields = ['due_back']
        labels = {'due_back': 'Renewal Date'}
        help_text = {'due_back': 'Enter a date between now and 4 weeks (default 3).'}


class AuthorDOBForm(forms.ModelForm):

    def cleaned_dob(self):
        date = self.cleaned_data['date_of_birth']

        if date > datetime.date.today():
            raise ValidationError('Invalid date - DOB in past')

        return date

    class Meta:
        model = Author
        fields = '__all__'